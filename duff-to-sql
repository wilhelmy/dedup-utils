#!/usr/bin/env perl
# Take the output from duff(1) and stuff it into a sqlite database for easier querying.
#
# For example, to count the number of duplicate files >50MB, use 
# SELECT COUNT(*) FROM dupes d LEFT JOIN cluster c ON d.cluster = c.id WHERE c.bytes > 1024*1024*50;
#
# Usage: duff -0 /path/to/files | duff-to-sql /tmp/dupes.db
#
# Performance is a lot faster if the sqlite database is located on a ramdisk.
# The resulting sqlite database file is >200MB in my case. The corresponding
# duff output file is 174MB. Make sure you have enough space available.
#
# Copyright © 2015 by Moritz Wilhelmy <mw@barfooze.de>
#
# You may copy and distribute this file under the same conditions as Perl 5
# itself. See the 'perlartistic' manpage for more information.

use strict;
use warnings;
use DBI;

INIT {
$/ = "\0";
#$\ = "\n";
}

my $db = shift(@ARGV) || "dupes.db";
my $dbh = DBI->connect("dbi:SQLite:$db", '', '', {RaiseError => 1});

# I suppose we can do without foreign keys in this case.
# Might be a bit faster.
#$dbh->do('PRAGMA foreign_keys = ON;');

# It didn't work with only one call to $dbh->do. *shrug*
$dbh->do(<<SQL)
CREATE TABLE IF NOT EXISTS cluster(
	  id      INTEGER PRIMARY KEY
	, bytes   INTEGER NOT NULL
	, hash    VARCHAR(255) UNIQUE NOT NULL
);
SQL
and $dbh->do(<<SQL);

CREATE TABLE IF NOT EXISTS dupes(
	  cluster INTEGER
	, path    TEXT NOT NULL
	, FOREIGN KEY (cluster) REFERENCES cluster(id)
);
SQL

$SIG{INT} = sub { $dbh->disconnect; print "\nInterrupted\n"; exit 1; };

my $st1 = $dbh->prepare('INSERT INTO cluster(id,bytes,hash) VALUES(?,?,?);');
my $st2 = $dbh->prepare('INSERT INTO dupes(cluster,path) VALUES(?,?);');

my $cur;
while (<>) {
	chomp;
	# Input format:
	#  cluster descriptor\0
	#  filename\0
	#  filename\0
	# ...
	if (/^(\d+) files in cluster (\d+) \((\d+) bytes, digest ([\da-f]+)\)$/) {
		$st1->execute($2, $3, $4);
		$cur = $2;
		print "\rCluster: $cur";
		STDOUT->flush;
	} elsif (defined $cur) {
		$st2->execute($cur, $_);
	} else {
		die "Malformed duff output, no cluster descriptor found: $_"
	}
}

print "\n";
